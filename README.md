# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME:** RUSLAN BAKHTIYAROV

**E-MAIL:** rusya.vay@mail.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10 x64

# HARDWARE
- CPU: Intel® Core™ i5-8265U
- RAM: 8GB

# PROGRAM RUN

```bash
java -jar ./task-manager-1.0.0.jar
```
# SCREENSHOTS
https://yadi.sk/d/Yzh4rmppoi2fRw?w=1